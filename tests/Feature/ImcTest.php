<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

use App\Models\Imc;
use App\Models\Usuario;

class ImcTest extends TestCase
{

    use RefreshDatabase;

    /**
     * A basic feature test example.
     * @test
     * @return void
     */
    public function new_imc()
    {
        $usuario = factory(Usuario::class)->create([
            'usuario' => 'loco11'
        ]);

        factory(Imc::class)->create([
            'peso_lb' => 105,
            'usuario_id' => $usuario->usuario
        ]);

        $response = $this->get("/imcs/details/loco11");

        $response
            ->assertStatus(200)
            ->assertSee("105");
    }
}
