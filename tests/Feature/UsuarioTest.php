<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

use App\Models\Usuario;

class UsuarioTest extends TestCase
{
    use RefreshDatabase;
    /**
     * @test
     * @return void
     */
    public function usuario_insercion()
    {
        $nombre = 'Keylah Johnes';

        factory(Usuario::class)->create([
            'nombre' => $nombre
        ]);

        $this->get('/usuarios')
            ->assertStatus(200)
            ->assertSee($nombre);
    }

    /**
     * @test
     * @return void
     */
    public function usuario_vacio(){
        $this->get('/usuarios')
            ->assertSee('No hay usuarios registrados');
    }
}
