@extends('layouts.app')

@section('title', 'IMC Listing')

@section('content')
  <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.min.js"></script>

  <h2>¡Bienvenido, {{$nombre_usuario}}! </h2>

  <a href="{{ action('IMCController@show', ['id' => $usuario_id]) }}"> 
    Mostrar Informacion Tabular 
  </a>

  <div class="col-lg-12 col-sm-12 col-md-12">
    <canvas id="lineChart"></canvas>
  </div>

  <script type="text/javascript">
    window.data = {{ $imcs }}
    // Prevent escaping quotes with this method
    window.labels = {{ new Illuminate\Support\HtmlString($labels) }}
  </script>

  <script src="/js/imc/index.js"></script>
    
@endsection