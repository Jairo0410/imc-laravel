@extends('layouts.app')

@section('title', 'IMC Listing')

@section('content')
  <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css" />
  <script src="//cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>

  <h2>¡Bienvenido, {{$nombre_usuario}}! </h2>

  <br>

  <a href="{{ action('IMCController@index', ['id' => $usuario_id]) }}"> 
    Mostrar Informacion Grafica 
  </a>

  <br>

  @empty($rows)
    <h2>No hay datos disponibles</h2>
  @else
    <table id="imcTable" class="table table-sm">
      <thead>
        <th>Fecha</th>
        <th>Altura (cm)</th>
        <th>Peso (lb)</th>
      </thead>
      <tbody>
      @foreach ($rows as $row)
        <tr>
          <td> {{ $row->fecha }} </td>
          <td> {{ $row->peso_lb }} </td>
          <td> {{ $row->altura_cm }} </td>
        </tr>
      @endforeach
      </tbody>
    </table>
  @endempty

  <script type="text/javascript">
    jQuery.noConflict();
    jQuery(document).ready(function () {
      jQuery('#imcTable').DataTable({
        "pagingType": "numbers"
      });
      jQuery('.dataTables_length').addClass('bs-select');
    });
  </script>
    
@endsection