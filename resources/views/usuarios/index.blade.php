@extends('layouts.app')

@section('content')

<h2>Listado de usuarios registrados</h2>

@if($usuarios->isEmpty())
    <h2>No hay usuarios registrados</h2>
@else
  <table class="table">
    <thead>
      <th>Usuario</th>
      <th>Nombre</th>
    </thead>
    <tbody>
      @foreach ($usuarios as $usuario)
      <tr>
        <td> {{$usuario->usuario}} </td>
        <td> {{$usuario->nombre}} </td>
      </tr>
      @endforeach
    </tbody>
  </table>
@endif

@endsection