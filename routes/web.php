<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', "WelcomeController");

Route::get('/usuarios', 'UsuarioController@index');
Route::get('/api/usuarios', 'UsuarioController@api_index');

Route::post('/login', 'UsuarioController@login');

Route::get("/imcs/{usuario}", "IMCController@index");
Route::get("/imcs/details/{usuario}", "IMCController@show");

Route::get("/usuarios/{id}", function ($id) {
    return "Informacion el usuario de identificador {$id}";
});
