<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Usuario extends Model
{
    protected $primaryKey = 'usuario';
    public $timestamps = false;
    public $incrementing = false;

    /**
     * Support for massive storage
     */
    protected $fillable = [
        'usuario',
        'nombre',
        'password'
    ];


    /**
     * Fields to hide when exporting object
     */
    protected $hidden = [
        'password'
    ];

    public function imcs(){
        return $this->hasMany(Imc::class, 'usuario_id');
    }
}
