<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Imc extends Model
{
    protected $primaryKey = 'fecha';
    public $timestamps = false;
    public $incrementing = false;

    public function usuario() {
        return $this->belongsTo(Usuario::class, 'usuario_id');
    }
}
