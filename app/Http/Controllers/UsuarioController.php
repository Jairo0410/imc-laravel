<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Usuario;

class UsuarioController extends Controller
{

    public function api_index(){
        $usuarios = Usuario::all();

        return response( json_encode($usuarios) )
            ->header("Content-Type", "application/json");
    }

    public function index(){
        $usuarios = Usuario::all();

        echo session('username', 'no username');

        return view('usuarios.index')
            ->with('usuarios', $usuarios);
    }

    public function login(Request $request){
        $username = $request->input('username');
        $password = $request->input('password');

        if($password == 'pass'){
            session(['username' => $username]);
            return response('Successful login ' . $username);
        }
        
        return response('Failed to login');
    }
}
