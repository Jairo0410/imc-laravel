<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Usuario;
use App\Models\Imc;

class IMCController extends Controller
{
    public function index($username){
        $usuario = Usuario::find($username);

        if($usuario){
            $rows = $usuario->imcs()
                ->orderBy('fecha', 'asc')
                ->get();

            $fechas = $rows->pluck('fecha');
            $imcs = $rows->map(function($imc){
                return (10000 * $imc->peso_lb) / (2.205 * pow($imc->altura_cm, 2));
            });

            return view('imcs.index')
                ->with('labels', $fechas)
                ->with('imcs', $imcs)
                ->with('nombre_usuario', $usuario->nombre)
                ->with('usuario_id', $username);
        }else{
            return view('imcs.index')
                ->with('labels', json_encode([]))
                ->with('imcs', json_encode([]))
                ->with('nombre_usuario', 'No disponible')
                ->with('usuario_id', $username);
        }
    }

    public function show($username){
        $usuario = Usuario::find($username);

        if($usuario){
            $rows = $usuario->imcs()
            ->orderBy('fecha', 'asc')
            ->get();

            return view('imcs.show')
                ->with('rows', $rows)
                ->with('nombre_usuario', $usuario->nombre)
                ->with('usuario_id', $username);
        }else{
            return view('imcs.show')
            ->with('rows', [])
            ->with('nombre_usuario', 'No disponible')
            ->with('usuario_id', $username);
        }
    }
}
