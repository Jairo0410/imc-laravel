<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\Imc;
use App\Models\Usuario;

class IMCSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('imcs')->truncate();

        /*
        $user = DB::table('usuarios')
            ->select('usuario')
            ->first();
        */

        $user = Usuario::select('usuario')
            ->first();

        Imc::create([
            'fecha' => '2019-12-03',
            'altura_cm' => 174,
            'peso_lb' => 126.32497927653,
            'usuario_id' => $user->usuario
        ]);

        Imc::create([
            'fecha' => '2019-10-11',
            'altura_cm' => 173,
            'peso_lb' => 127.427291486622,
            'usuario_id' => $user->usuario
        ]);

        Imc::create([
            'fecha' => '2019-11-01',
            'altura_cm' => 173,
            'peso_lb' => 122.577117762218,
            'usuario_id' => $user->usuario
        ]);

        factory(Imc::class)
            ->times(400)
            ->create();
    }
}
