<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\Usuario;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('usuarios')->truncate();

        Usuario::create([
            'usuario' => 'jolaf',
            'nombre' => 'Jose Omar Lopez Fonseca',
            'password' => bcrypt('frozenpal')
        ]);

        Usuario::create([
            'usuario' => 'hect14', 
            'nombre' => 'Hector Perez Sibaja', 
            'password' => bcrypt('honey')
        ]);

        /*
        DB::table('usuarios')->insert([
            'usuario' => 'jolaf',
            'nombre' => 'Jose Omar Lopez Fonseca',
            'password' => bcrypt('frozenbitch')
        ]);
        */

        /* Create 20 random users using factories at 
        app/database/factory */
        factory(Usuario::class)
            ->times(20)
            ->create();
    }
}
