<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(App\Models\Imc::class, function (Faker $faker) {
    $rand_user = App\Models\Usuario::inRandomOrder()
        ->first()
        ->usuario;

    return [
        'fecha' => $faker->date,
        'altura_cm' => $faker->numberBetween($min = 50, $max = 195),
        'peso_lb' => $faker->randomFloat($nbMaxDecimals = 3, $min = 44.092, $max = 396.832),
        'usuario_id' => $rand_user
    ];
});
