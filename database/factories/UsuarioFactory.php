<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(App\Models\Usuario::class, function (Faker $faker) {
    return [
        'usuario' => $faker->userName,
        'nombre' => $faker->name,
        'password' => bcrypt(str_random(10))
    ];
});
